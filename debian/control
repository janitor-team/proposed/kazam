Source: kazam
Section: video
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andrew Starr-Bochicchio <asb@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               gettext,
               intltool,
               python3-all (>= 3.2),
               python3-distutils-extra,
               python3-setuptools
Standards-Version: 4.1.4
Vcs-Git: https://salsa.debian.org/python-team/packages/kazam.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/kazam
Homepage: https://launchpad.net/kazam

Package: kazam
Architecture: all
Depends: gir1.2-gst-plugins-base-1.0,
         gir1.2-gstreamer-1.0,
         gir1.2-keybinder-3.0,
         gir1.2-wnck-3.0,
         gnome-session-canberra,
         gstreamer1.0-plugins-base,
         gstreamer1.0-plugins-good (>= 1.14.0-1),
         gstreamer1.0-plugins-ugly,
         gstreamer1.0-pulseaudio,
         python3-cairo,
         python3-dbus,
         python3-gi,
         python3-gi-cairo,
         python3-xdg,
         ${dist:Depends},
         ${misc:Depends},
         ${python3:Depends}
Recommends: gstreamer1.0-libav
XB-Python-Version: ${python:Versions}
Description: screencast and screenshot application created with design in mind
 Kazam provides a well designed and easy to use interface for capturing
 screencasts and screenshots. It can record desktop video and multiple audio
 streams simultaneously with control over audio levels and the screen region
 being captured.
 .
 Support for H264 and VP8 codecs is built in.
